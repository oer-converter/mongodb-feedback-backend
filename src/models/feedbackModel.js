const mongoose = require('mongoose');

var feedbackModel = mongoose.model('feedback', {
	wasHelpful: { type: String },
	uiIsGood: { type: String },
	iWillUseIt: { type: String },
	recommendOthers: { type: String },
	suggestions: { type: String }
});

module.exports =  { feedbackModel };