const mongoose = require ('mongoose');


MongoUrl = 'mongodb+srv://OerDev:***REMOVED***@cluster0.hhifr.mongodb.net/Feedback?retryWrites=true&w=majority'; // Use it when in Production... DB is live... 

// MongoUrl = 'mongodb://mongo:27017/docker-node-mongo';   // Use this when build images with docker-compose file...
// MongoUrl = 'mongodb://localhost:27017/Feedback';   // Uncomment it when testing locally...


mongoose.connect(MongoUrl, (err) => {
	if (!err)
		console.log('MongoDB connection successful...');
	else
		console.log('Error in DB connection: ' + JSON.stringify(err, undefined, 2));
});

module.exports = mongoose;


